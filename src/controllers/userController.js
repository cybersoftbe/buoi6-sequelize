// error 200, 400, 500

// const User = require("../models/user");
const sequelize = require("../models/index");
const initModels = require("../models/init-models");

const model = initModels(sequelize);

// get all
const getUser = async (req, res) => {
  try {
    // SELECT * FROM user;
    let data = await model.user.findAll(); // --> list object [{}]

    res.status(200).send(data);
  } catch (err) {
    res.status(500).send("Lỗi server!");
  }
};

// get id
const getUserId = async (req, res) => {
  try {
    let { id } = req.params;

    let dataOne = await model.user.findOne({
      where: {
        user_id: id,
      },
    }); // --> object {}

    if (dataOne) res.status(200).send(dataOne);
    else res.status(400).send("User không tồn tại!");
  } catch (err) {
    res.status(500).send("Lỗi server!");
  }
};

const createUser = async (req, res) => {
  try {
    let { full_name, email, pass_word } = req.body;

    let payload = {
      full_name,
      email,
      pass_word,
    };

    let data = await model.user.create(payload);
    if (data) res.status(200).send("Thêm user thành công!");
  } catch (err) {
    res.status(500).send("Lỗi server!");
  }
};

const updateUser = async (req, res) => {
  try {
    let { id } = req.params;

    let dataOne = await model.user.findOne({
      where: {
        user_id: id,
      },
    });

    if (dataOne) {
      let { full_name, email, pass_word } = req.body;

      let payload = {
        full_name,
        email,
        pass_word,
      };

      // UPDATE user SET ... WHERE user_id = id
      let data = await model.user.update(payload, {
        where: {
          user_id: id,
        },
      });

      res.status(200).send("Cập nhật user thành công!");
    } else res.status(400).send("User không tồn tại!");
  } catch (err) {
    res.status(500).send("Lỗi server!");
  }
};

const deleteUser = async (req, res) => {
  try {
    let { id } = req.params;

    let dataOne = await model.user.findOne({
      where: {
        user_id: id,
      },
    });

    if (dataOne) {
      await model.user.destroy({
        where: {
          user_id: id,
        },
      });

      res.status(200).send("Xóa user thành công!");
    } else res.status(400).send("User không tồn tại!");
  } catch (err) {
    res.status(500).send("Lỗi server!");
  }
};

module.exports = { getUser, getUserId, createUser, updateUser, deleteUser };
