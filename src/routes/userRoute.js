const express = require("express");
// userRoute object
const userRoute = express.Router();
const {
  getUser,
  getUserId,
  createUser,
  updateUser,
  deleteUser,
} = require("../controllers/userController");

// GET
userRoute.get("/getUser/", getUser); // get all
userRoute.get("/getUserId/:id", getUserId); // get id

// POST
userRoute.post("/createUser", createUser);

// PUT
userRoute.put("/updateUser/:id", updateUser);

// DELETE
userRoute.delete("/deleteUser/:id", deleteUser);

module.exports = userRoute;
